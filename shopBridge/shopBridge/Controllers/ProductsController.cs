﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNet.OData;
using shopBridge.Models;
using System.Text;

namespace shopBridge.Controllers
{
    public class ProductsController : ODataController
    {
        private ProductEntities db = new ProductEntities();

        //constructor
        public ProductsController() { }

        //Uncomment to run test cases
        //private IProduct_UnitTestAppContext db = new Product_UnitTestAppContext();

        //public ProductsController(IProduct_UnitTestAppContext context)
        //{
        //    db = context;
        //}

        // GET: odata/GetAllProducts
        [HttpGet]
        [ODataRoute("GetAllProducts")]
        public IQueryable<Product> GetAllProducts()
        {
            return db.Products;
        }

        // GET: odata/GetProduct(ProductId=5)
        [HttpGet]
        [ODataRoute("GetProduct(ProductId={ProductId})")]
        public async Task<IHttpActionResult> GetProduct([FromODataUri] int ProductId)
        {
            Product product = await db.Products.FindAsync(ProductId);
            if(product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        // POST: odata/AddProduct
        [HttpPost]
        [ODataRoute("AddProduct")]
        public async Task<IHttpActionResult> AddProduct([FromBody]Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Validate(product);

            db.Products.Add(product);
            try
            {
                db.Entry(product).State = EntityState.Added;
                await db.SaveChangesAsync(); 
            }
            catch (DbUpdateException)
            {
                if (ProductExists(product.ProductId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return Created(product);
        }

        // PATCH: odata/UpdateProduct(ProductId=5)
        [AcceptVerbs("PATCH", "MERGE")]
        [ODataRoute("UpdateProduct(ProductId={ProductId})")]
        public async Task<IHttpActionResult> UpdateProduct([FromODataUri] int ProductId, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Validate(product);

            Product productDetail = await db.Products.FindAsync(ProductId);
            if (productDetail == null)
            {
                return NotFound();
            }

            productDetail.ProductName = product.ProductName;
            productDetail.Category = product.Category;
            productDetail.Description = product.Description;
            productDetail.Price = product.Price;
            productDetail.Quantity = product.Quantity;

            db.Products.Attach(productDetail);
       
            try
            {
                db.Entry(productDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(ProductId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Updated(product);
        }

        // DELETE: odata/DeleteProduct(ProductId=5)
        [AcceptVerbs("DELETE")]
        [ODataRoute("DeleteProduct(ProductId={ProductId})")]
        public async Task<IHttpActionResult> DeleteProduct([FromODataUri] int ProductId)
        {
            Product product = await db.Products.FindAsync(ProductId);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(ProductId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int ProductId)
        {
            return db.Products.Count(e => e.ProductId == ProductId) > 0;
        }

        private static void Validate(Product product)
        {
            var errors = new List<string>();
            if (product.ProductName == null || product.ProductName == "")
            {
                errors.Add("Product Name is mandatory !");
            }
            if (product.Category == null || product.Category == "")
            {
                errors.Add("Product Category is mandatory!");
            }
            if (product.Description == null || product.Description == "")
            {
                errors.Add("Product Description is mandatory!");
            }
            if (product.Price == null)
            {
                errors.Add("Product Price is mandatory!");
            }
            if (product.Price != null && product.Price < 0)
            {
                errors.Add("Invalid Price! Product Price cannot take a negative value");
            }
            if (product.Quantity == null)
            {
                errors.Add("Product Quantity is mandatory!");
            }
            if (product.Quantity != null && product.Quantity < 0)
            {
                errors.Add("Invalid Quantity! Product Quantity cannot take a negative value");
            }
            if (errors.Any())
            {
                var errorBuilder = new StringBuilder();

                errorBuilder.AppendLine("Invalid values, reason: ");

                foreach (var error in errors)
                {
                    errorBuilder.AppendLine(error);
                }

                throw new Exception(errorBuilder.ToString());
            }
        }
    }
}
