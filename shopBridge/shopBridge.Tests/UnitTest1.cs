﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using shopBridge.Models;
using shopBridge.Controllers;
using System.Collections.Generic;
using System.Web.Http.Results;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData.Results;
using System.Web.Http;
using System.Net;
using System.Threading;
using System.Net.Http;

namespace shopBridge.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetAllProducts_ShouldReturnAllProducts()
        {
            //Arrange
            var context = new TestProductAppContext();
            context.Products.Add(new Product { ProductId = 1, ProductName = "Demo1", Category = "Category1", Description = " Description1", Price = 1000, Quantity = 1 });

            context.Products.Add(new Product { ProductId = 2, ProductName = "Demo2", Category = "Category2", Description = "Description2", Price = 20000, Quantity = 2 });

            context.Products.Add(new Product { ProductId = 3, ProductName = "Demo3", Category = "Category3", Description = "Description3",  Price = 30000, Quantity = 3 });

            //Act
            var controller = new ProductsController(context);
            var result = controller.GetAllProducts() as TestProductDbSet;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Local.Count);
        }

        [TestMethod]
        public void PostProduct_ShouldReturnProduct()
        {
            //Arrange
            var controller = new ProductsController(new TestProductAppContext());
            var item = GetTestProduct();

            //Act
            var task = controller.AddProduct(item).Result;
            var result = (task as CreatedODataResult<Product>).Entity;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(item.ProductName,result.ProductName);
        }

        [TestMethod]
        public void UpdateProduct_ShouldReturn204Status()
        {
            //Arrange
            var controller = new ProductsController(new TestProductAppContext());
            var item = GetTestProduct();
            var addTask = controller.AddProduct(item).Result;
            var product = (addTask as CreatedODataResult<Product>).Entity;
            var updateItem = new Product() { ProductId = 1, ProductName = "UpdatedName", Category = "UpdatedCategory", Description = " UpdatedDescription", Price = 1000, Quantity = 1 };

            //Act
            var task = controller.UpdateProduct(product.ProductId, updateItem).Result;
            var result = (task as UpdatedODataResult<Product>).Entity;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(updateItem.ProductName, result.ProductName);
        }

        [TestMethod]
        public async Task UpdateProduct_ShouldReturn404Code()
        {
            //Arrange
            var controller = new ProductsController(new TestProductAppContext());
            var item = GetTestProduct();

            //Act
            IHttpActionResult result = await controller.UpdateProduct(item.ProductId, item);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task DeleteProduct_ShouldReturn200Code()
        {
            //Arrange
            var controller = new ProductsController(new TestProductAppContext());
            var item = GetTestProduct();
            var task = controller.AddProduct(item).Result;
            var product = (task as CreatedODataResult<Product>).Entity;

            //Act
            var result = await controller.DeleteProduct(product.ProductId) as StatusCodeResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [TestMethod]
        public async Task DeleteProduct_ShouldReturn404Code()
        {
            //Arrange
            var controller = new ProductsController(new TestProductAppContext());
            var item = GetTestProduct();

            //Act
            IHttpActionResult result = await controller.DeleteProduct(item.ProductId);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        Product GetTestProduct()
        {
            return new Product() { ProductId = 10, ProductName = "Demo1", Category = "Category1", Description = " Description1", Price = 1000, Quantity = 1 };
        }
    }
}
