﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using shopBridge.Models;

namespace shopBridge.Models
{
    public class Product_UnitTestAppContext : DbContext, IProduct_UnitTestAppContext
    {
        public Product_UnitTestAppContext() : base("name=shopBridge.Models")
        {
        }
        public DbSet<Product> Products { get; set; }

        public new Task<int> SaveChangesAsync()
        {
            return Task.FromResult(0);
        }

    }
}