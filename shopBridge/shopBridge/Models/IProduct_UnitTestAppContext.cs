﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shopBridge.Models
{
    public interface IProduct_UnitTestAppContext : IDisposable
    {
        DbSet<Product> Products { get; }
        Task<int> SaveChangesAsync();
    }
}
