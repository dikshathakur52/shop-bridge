﻿using shopBridge.Models;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using System.Web.Http;
using System.Web.Http.OData.Batch;
using Microsoft.OData.Edm;
using System.Linq;
using System;
using System.Web;

namespace shopBridge
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            var model = GetModel();
            var odataBatchHandler = new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer);

            ODataModelBuilder builder = new ODataConventionModelBuilder();
            config.MapODataServiceRoute(
                routeName: "odata",
                routePrefix: "odata",
                model: model);

            config.EnsureInitialized();

            // Set global time zone info.
            config.SetTimeZoneInfo(TimeZoneInfo.Utc);
        }
        private static IEdmModel GetModel()
        {
            ODataModelBuilder builder = new ODataConventionModelBuilder();

            builder.Namespace = "shopBridge";

            builder.ContainerName = "Default";

            builder.EntitySet<Product>("Products").EntityType.HasKey(x=>x.ProductId);

            var getProductList = builder.Function("GetAllProducts").Returns<IQueryable<Product>>();

            var getProduct = builder.Function("GetProduct").ReturnsFromEntitySet<Product>("Products");
            getProduct.Parameter<int>("ProductId");

            var addProduct = builder.Function("AddProduct").ReturnsFromEntitySet<Product>("Products");

            var updateProduct = builder.Function("UpdateProduct").ReturnsFromEntitySet<Product>("Products");
            updateProduct.Parameter<int>("ProductId");

            var deleteProduct = builder.Function("DeleteProduct").Returns<IHttpActionResult>();
            deleteProduct.Parameter<int>("ProductId");

            return builder.GetEdmModel();
        }
    }
}
