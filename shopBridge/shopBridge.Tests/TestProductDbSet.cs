﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shopBridge.Models;

namespace shopBridge.Tests
{
    class TestProductDbSet : TestDbSet<Product>
    {
        public override async Task<Product> FindAsync(params object[] keyValues)
        {
            return await this.SingleOrDefaultAsync(product => product.ProductId == (int)keyValues.Single());
        }
    }

    class TestProductAppContext : IProduct_UnitTestAppContext
    {
        public TestProductAppContext()
        {
            this.Products = new TestProductDbSet();
        }

        public DbSet<Product> Products { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return Task.FromResult(0);
        }

        public void Dispose() { }
    }
}
